# data.europa.eu SPARQL by example

Endpoint: https://data.europa.eu/sparql

## Get graph name for each catalog
```
prefix dcat:  <http://www.w3.org/ns/dcat#>

SELECT ?g ?d
WHERE {
  GRAPH ?g { 
     ?d a dcat:Catalog.
  }
}
```

## Get a catalogue by graph name
```
CONSTRUCT
{
    GRAPH <http://data.europa.eu/88u/catalogue/london-datastore> {
        ?s ?p ?o
    }
}
WHERE
{
    GRAPH <http://data.europa.eu/88u/catalogue/london-datastore>  {
        ?s ?p ?o
    }
}
```


## Get a dataset by graph name
```
CONSTRUCT
{
    GRAPH <http://data.europa.eu/88u/dataset/green-roofs> {
        ?s ?p ?o
    }
}
WHERE
{
    GRAPH <http://data.europa.eu/88u/dataset/green-roofs>  {
        ?s ?p ?o
    }
}
```

## Get a metric by graph name
```
CONSTRUCT
{
    GRAPH <http://data.europa.eu/88u/metrics/green-roofs> {
        ?s ?p ?o
    }
}
WHERE
{
    GRAPH <http://data.europa.eu/88u/metrics/green-roofs>  {
        ?s ?p ?o
    }
}
```


## COVID-related datasets, where the actual data is available

```
prefix dcat:  <http://www.w3.org/ns/dcat#>
prefix dct:   <http://purl.org/dc/terms/>
prefix dqv: <http://www.w3.org/ns/dqv#>

SELECT ?d ?title ?dist ?v ?accessURL WHERE {
  ?d a dcat:Dataset .
  ?d dct:title ?title .
  ?d dcat:distribution ?dist .
  ?dist dqv:hasQualityMeasurement ?q .
  ?dist dct:format "CSV" .
  ?dist dcat:accessURL ?accessURL .
  ?q dqv:isMeasurementOf <https://piveau.eu/ns/voc#mediaTypeAvailability> .
  ?q dqv:value ?v .
  ?q dqv:value "true"^^<http://www.w3.org/2001/XMLSchema#boolean>
  filter contains(?title, "COVID-19")
}
LIMIT 10
```
