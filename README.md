# SemWebEGov
Website for the tutorial **Semantic Web for E-Government** at the **The 20th International Semantic Web Conference 2021**

Based on the original work from David Chaves-Fraga and Ana Iglesias-Molina on the KGC2020 tutorial at ISWC2020
